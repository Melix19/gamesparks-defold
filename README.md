# gamesparks-defold
This is the GameSparks SDK for the [Defold game engine](http://www.defold.com), based on the official GameSparks repository that has been deleted. This fork has fixes some of the bugs that were in the old repository to make this sdk working with the latest Defold versions.

## Installation
You can use the SDK in your own project by adding the gamesparks folder in your project and by adding the following dependencies in your project settings:
```
https://github.com/sonountaleban/defold-crypto/archive/master.zip
https://github.com/britzl/defold-luasec/archive/master.zip
https://github.com/britzl/defold-luasocket/archive/master.zip
```

You also need to add the following file in the custom resources field:
```
gamesparks/dmc_corona.cfg
```

## Usage
For the usage see the [GameSparks documentation](https://docs.gamesparks.com) please.

## Limitations
This module is available for all platforms that are currently supported by [Native Extensions](http://www.defold.com/manuals/extensions/).